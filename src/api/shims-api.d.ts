import { Card } from "../modules/shims-welcome-to"

type Round = {
    actions?: Array<Card | undefined>
    numbers?: Array<Card | undefined>
    turn: number
}

type Game = {
    name: string
    round?: Round
    cards?: Array<Round>
    owner: User
}

type User = {
    id: string
    email: string
    pseudo: string
}
