import firebase from '../modules/firebase'
import { User } from './shims-api'

export default class gamesController {
    private database

    constructor() {
        this.database = firebase.database()
    }

    async index(): Promise<firebase.database.DataSnapshot> {
        try {

            const snapshot = await this.database.ref('users').once('value')

            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }

    }

    async store(user: User): Promise<firebase.database.DataSnapshot> {
        try {
            const gameListRef = firebase.database().ref('users');
            const newGameRef = gameListRef.child(user.id)
            newGameRef.set({
                email: user.email,
                pseudo: user.pseudo
            })
            const snapshot = await newGameRef.once('value')
            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async show(id: string): Promise<firebase.database.DataSnapshot> {
        try {
            const snapshot = await this.database.ref(`users/${id}`).once('value')

            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async update(id: string, user: User) {
        const updates = {}
        updates[id] = user

        try {
            await this.database.ref(`users`).update(updates)
            const snapshot = await this.database.ref(`users/${id}`).once('value')

            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }



    }

    async destroy(id: string) {
        try {
            await this.database.ref(`users/${id}`).remove()

            return Promise.resolve()
        } catch (error) {
            return Promise.reject(error)
        }
    }

    watch(id, callback) {
        this.database.ref(`users/${id}`).on('value', (snap) => {
            callback(snap)
        })
    }

    watchAll(callback) {
        this.database.ref('users').on('value', (snap) => {
            callback(snap)
        })
    }
}
