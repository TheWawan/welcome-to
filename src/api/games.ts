import firebase from '../modules/firebase'
import { Game } from './shims-api'
export default class gamesController {
    private database

    constructor() {
        this.database = firebase.database()
    }

    async index(): Promise<firebase.database.DataSnapshot> {
        try {

            const snapshot = await this.database.ref('games').once('value')

            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }

    }

    async store(game: Game): Promise<firebase.database.DataSnapshot> {
        try {
            const gameListRef = firebase.database().ref('games');
            const newGameRef = gameListRef.push();
            newGameRef.set(game)
            const snapshot = await newGameRef.once('value')
            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async show(id: string): Promise<firebase.database.DataSnapshot> {
        try {
            const snapshot = await this.database.ref(`games/${id}`).once('value')

            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async update(id: string, game: Game) {
        const updates = {}
        updates[id] = game

        try {
            await this.database.ref(`games`).update(updates)
            const snapshot = await this.database.ref(`games/${id}`).once('value')
            
            return Promise.resolve(snapshot)
        } catch (error) {
            return Promise.reject(error)
        }


        
    }

    async destroy(id: string) {
        try {
            await this.database.ref(`games/${id}`).remove()

            return Promise.resolve()
        } catch (error) {
            return Promise.reject(error)
        }
    }

    watch(id, callback) {
        this.database.ref(`games/${id}`).on('value', (snap) => {
            callback(snap)
        })
    }

    watchAll(callback) {
        this.database.ref('games').on('value', (snap) => {
            callback(snap)
        })
    }
}
