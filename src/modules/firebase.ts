// Import Firebase
import firebase from "firebase/app"

// Add the Firebase services that you want to use
import "firebase/auth"
import "firebase/database"

import { firebaseConfig } from '../config/firebase-config'

firebase.initializeApp(firebaseConfig)

export default firebase

