import { Cards, Decks } from "./shims-welcome-to"
import welcomeToCards from "../resources/cards.json"

export const cards: Cards = welcomeToCards


/**
 * Shuffle function
 *
 * inspired by the Fisher–Yates shuffle
 * https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Fisher_and_Yates'_original_method
 *
 * 1. Write down the numbers from 1 through N.
 * 2. Pick a random number k between one and the number of unstruck numbers remaining (inclusive).
 * 3. Counting from the low end, strike out the kth number not yet struck out, and write it down at the end of a separate list.
 * 4. Repeat from step 2 until all the numbers have been struck out.
 * 5. The sequence of numbers written down in step 3 is now a random permutation of the original numbers.
 *
 * @param cards Cards
 *
 * @returns shuffled Cards
 */
export function shuffle(cards: Cards): Cards {
  const shuffled: Cards = []

  while (cards.length) {
    const randomNumber: number = Math.floor(Math.random() * cards.length)
    shuffled.push(cards.splice(randomNumber, 1)[0])
  }

  return shuffled
}

/**
 * Deal the cards to decks
 *
 * @param cards Card
 * @param decksCount Number
 *
 * @returns decks Decks
 */
export function getDecks(cards: Cards, decksCount: Number): Decks {
  const decks = []

  while (cards.length) {
    for (let i = 0; i < decksCount; i++) {
      if (!decks[i]) {
        decks[i] = []
      }

      decks[i].push(cards.pop())
    }
  }

  return decks
}
