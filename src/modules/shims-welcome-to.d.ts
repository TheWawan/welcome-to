export type Card = {
    number: Number,
    action: String
}

export type Cards = Array<Card>

export type Decks = Array<Cards>
