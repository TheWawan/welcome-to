import { Game } from './api/shims-api'
import { Card } from './modules/shims-welcome-to'

import gameApi from './api/games'
const gamesController = new gameApi()

gamesController.index().then(snapshot => {
    console.log('index:')
    console.log(snapshot.val())
})

gamesController.show('MMXmNiArRYO7Al5qjFi').then(snapshot => {
    console.log('show:')
    console.log(snapshot.val())
})

const game: Game = {
    name: 'test',
    history: []
}

gamesController.store(game).then(snapshot => {
    console.log('store:')
    console.log(snapshot.val())
    const id = snapshot.key

    gamesController.show(id).then(snapshot => {
        console.log('show:')
        console.log(snapshot.val())
    })

    const game: Game = {
        name: 'updated',
        history: []
    }
    gamesController.update(id, game).then(snapshot => {
        console.log('update:')
        console.log(snapshot.val())

        // gamesController.destroy(id).then(snapshot => {
        //     console.log('delete: true')
        // })
    })
})
