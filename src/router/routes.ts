import Login from '../pages/Login.vue'
import Home from '../pages/Home.vue'
import Game from '../pages/Game.vue'
import Games from '../pages/Games.vue'

// 2. Define some routes
// Each route should map to a component.
// We'll talk about nested routes later.
const routes = [
    { name: 'Login', path: '/login', component: Login, meta: { layout: 'simple' }},
    { name: 'Home', path: '/', component: Home, meta: { layout: 'default' } },
    { name: 'Game', path: '/game/:gameId', component: Game, meta: { layout: 'default' }, props: true },
    { name: 'Games', path: '/games', component: Games, meta: { layout: 'default' } },
]

export default routes
