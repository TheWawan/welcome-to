import { createRouter, createWebHashHistory } from 'vue-router'
import { useStore } from 'vuex'
import store from '../store'

import routes from './routes'

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

console.log(store)

let isAuthenticated = store.getters['user/isAuthenticated']

router.beforeEach((to, from, next) => {
    isAuthenticated = store.getters['user/isAuthenticated']
    if (to.name !== 'Login' && !isAuthenticated) next({ name: 'Login' })
    else next()
})

export default router
