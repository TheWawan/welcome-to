const user = {
    namespaced: true,
    state: () => ({
        user: {}
    }),
    mutations: {
        set_user(state, user) {
            state.user = user
        },
    },
    getters: {
        getUser: state => state.user,
        isAuthenticated: state => Object.keys(state.user).length ? true : false
    }
}

export default user
